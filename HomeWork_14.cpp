﻿#include <iostream>
#include <string>

int main()
{
    std::string Text("My HomeWork for Skillbox");
    std::cout << "Text: " << Text << std::endl;
    std::cout << "Length of Text: " << Text.length() << std::endl;
    std::cout << "First symbol: " << Text[0] << std::endl;
    std::cout << "Last symbol: " << Text[Text.length() - 1] << std::endl;
}
